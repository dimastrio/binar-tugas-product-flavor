package id.dimas.tugasproductflavor.helper

import android.content.Context
import id.dimas.tugasproductflavor.database.NoteDatabase
import id.dimas.tugasproductflavor.database.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NoteRepo(context: Context) {

    private val mDb = NoteDatabase.getInstance(context)

    suspend fun getNote(user: User) = withContext(Dispatchers.IO) {
        mDb?.noteDao()?.getAllNote(userId = user.userId ?: 0)
    }


}